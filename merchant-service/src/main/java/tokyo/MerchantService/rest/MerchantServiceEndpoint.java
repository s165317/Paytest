package tokyo.MerchantService.rest;

/**
 * @author Atli
 */

import java.io.File;

import javax.ws.rs.*;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import database.MerchantCollection;
import logic.Merchant;
import logic.MerchantManager;


@Path("/merchant")
public class MerchantServiceEndpoint {
    private static MerchantManager merchantManager = new MerchantManager(new MerchantCollection());

    @POST
    @Path("/registration")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response registerMerchant(Merchant mr) {
        try {
            String response = Boolean.toString(merchantManager.registerMerchant(mr));
        	//boolean response = customerManager.registerCustomer(cr);
            //System.out.println("Server side: " + response);
            return Response.status(201).entity(response).build();

        } catch (Exception e) {
            throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
        }
    }
    
    @POST
    @Path("/payment")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response requestPayment(@QueryParam("tokenID") String tokenID,@QueryParam("amount") int amount, Merchant merchant) {
        try {
            String response = Boolean.toString(merchantManager.requestPayment(tokenID,amount,merchant));
            //String response = merchantManager.requestPayment(tokenID,amount,merchant);
            //System.out.println("Server side: " + response);
            return Response.status(201).entity(response).build();

        } catch (Exception e) {
            throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
        }
    }
    
    
//    @POST
//    @Path("/barcode")
//    @Consumes(MediaType.APPLICATION_OCTET_STREAM)
//    @Produces(MediaType.APPLICATION_JSON)
//    public Response getBarcode(File barcode) {
//
//        try {
//        	//barcode = new File("newFile.png");
//            return Response.status(201).entity(barcode.getAbsolutePath() + "  merchServ").build();
//
//        } catch (Exception e) {
//            throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
//        }
//    }
    
//    @GET
//    @Produces(MediaType.APPLICATION_OCTET_STREAM)
//    public Response getFile() {
//      File file = null; // Initialize this to the File path you want to serve.
//      return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM).header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
//          .build();
//    }
    
//Path("/test")
//    @POST
//  //  @Produces("text/plain")
//    @Consumes(MediaType.APPLICATION_JSON)
//    public Response registerCustomer2(Customer cr){
//        try {
//            String response = merchantManager.registerMerchant(cr) + "size: " + merchantManager.getDatabase().size();
//            //System.out.println("Server side: " + response);
//            return Response.status(201).entity(response).build();
//
//        } catch (Exception e) {
//            throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
//        }
//    }

    @Path("/test")
    @GET
    @Produces("text/plain")
    public Response doGet(){
        return  Response.ok("Hello there Mr. Merchant!").build();
    }

//    @Path("/use/{tokenid}")
//    @POST
//    @Consumes(MediaType.APPLICATION_JSON)
//    @Produces(MediaType.APPLICATION_JSON)
//    public String useToken(@PathParam("tokenid") String tokenid) {
//        try {
//            return "The message is: " + tokenid;
//        } catch (Exception e) {
//            throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
//        }
//    }
}