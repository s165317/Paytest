package logic;
/**
 * @author Clara
 */

import java.math.BigDecimal;
import java.util.ArrayList;

import java.util.List;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import database.MerchantRegister;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;

public class MerchantManager {
    private MerchantRegister merchantRegister;

    public MerchantManager(MerchantRegister merchantRegister){
        this.merchantRegister = merchantRegister;
    }

//    public List<Merchant> getDatabase() {
//        return database;
//    }

//    public void setDatabase(List<Merchant> database) {
//        this.database = database;
//    }

    public boolean registerMerchant(Merchant merchant){
        try{
            merchantRegister.storeMerchant(merchant);
            return true;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public boolean removeMerchant(Merchant merchant){
        try{
            merchantRegister.removeMerchant(merchant.getCprNumber());
            return true;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

	public boolean requestPayment(String tokenID, int amount,Merchant merchant) {
		BankService bank = new BankServiceService().getBankServicePort();
		Client c = ClientBuilder.newClient();
		WebTarget r = c.target("http://02267-tokyo.compute.dtu.dk:8585");
		Response requestCPR = r.path("tokens").path("status").queryParam("tokenID", tokenID).request().post(Entity.json(""));
		// requestResponse is CPR right now
		String CPRResponse = requestCPR.readEntity(String.class);
        requestCPR.close();
//      return CPRResponse;
        WebTarget r1 = c.target("http://02267-tokyo.compute.dtu.dk:8484");
       	Response requestAccount = r1.path("customer").path("account").request().post(Entity.json(CPRResponse));
       	String AccountResponse = requestAccount.readEntity(String.class);
        requestAccount.close();
//        return AccountResponse;
        try {
			bank.transferMoneyFromTo(AccountResponse, merchant.getAccount(), new BigDecimal(amount), "100 kroner has been transfered");
			return true;
		} catch (BankServiceException_Exception e) {
			throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
		}
	}
}
