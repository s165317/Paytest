package database;
/**
 * @author Clara
 */

import java.util.ArrayList;

import logic.Merchant;

public interface MerchantRegister {
	public void storeMerchant(Merchant merchant);
	//public boolean updateCustomer(Customer customer);
	//public boolean containsCustomer(String customerID);
	public Merchant getMerchant(String merchantID);
	public void removeMerchant(String merchantID);
	//public ArrayList<TokenRepresentation> retrieveTokens(String customerID); 
}
