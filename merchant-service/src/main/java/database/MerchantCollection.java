package  database;

/**
 * @author Atli
 */
import java.util.ArrayList;

import java.util.HashMap;

import logic.Merchant;

public class MerchantCollection implements MerchantRegister {
	HashMap<String, Merchant> merchants;

	public MerchantCollection() {
		merchants = new HashMap<>();
	}

	@Override
	public void storeMerchant(Merchant m) {
		merchants.put(m.getCprNumber(), m);
	}
	
	@Override
	public Merchant getMerchant(String merchantID) {
		return merchants.get(merchantID);
	}

	@Override
	public void removeMerchant(String merchantID) {
		merchants.remove(merchantID);
		
	}
}
