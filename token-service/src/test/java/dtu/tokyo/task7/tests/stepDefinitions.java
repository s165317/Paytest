//package dtu.tokyo.task7.tests;
//
//import static org.junit.Assert.assertEquals;
//
//import static org.junit.Assert.assertFalse;
//import static org.junit.Assert.assertTrue;
//
//import java.util.Iterator;
//
//import cucumber.api.PendingException;
//import cucumber.api.java.en.Given;
//import cucumber.api.java.en.Then;
//import cucumber.api.java.en.When;
//import dtu.tokyo.task7.logic.BarcodeGenerator;
//import dtu.tokyo.task7.logic.Customer;
//import dtu.tokyo.task7.logic.Token;
//import dtu.tokyo.task7.logic.TokenManager;
//
//public class stepDefinitions{
//	Customer customer;
//	TokenManager tokenManager= new TokenManager();
//	Boolean response;
//	String tokenID;
//	BarcodeGenerator barcodeGenerator=new BarcodeGenerator();
//	String name;
//	int numberOfTokens;
//
//
//	@Given("^a customer has a valid token$")
//	public void aCustomerHasAValidToken() {
//		Customer customer = new Customer("Anna","Hansen");
//		tokenManager.requestTokens(customer, 1);		
//		tokenID=customer.getTokens().iterator().next().getTokenID();
//		assertTrue(tokenManager.getTokenRegister().getStatusOfToken(tokenID));
//	}
//
//	@When("^the customer use the token$")
//	public void theCustomerUseTheToken() {
//		response=tokenManager.useToken(tokenID);
//	}
//
//	@Then("^the token is accepted$")
//	public void theTokenIsAccepted() {
//		assertTrue(response);
//	}
//
//	@Then("^the token is registered as used$")
//	public void theTokenIsRegisteredAsUsed() throws Throwable {
//		assertFalse(tokenManager.getTokenRegister().getStatusOfToken(tokenID));
//	}
//
//	@Given("^a customer has a used token$")
//	public void aCustomerHasAUsedToken() throws Throwable {
//		Customer customer = new Customer("Ben");
//		tokenManager.requestTokens(customer, 1);
//		tokenID=customer.getTokens().iterator().next().getTokenID();
//		tokenManager.useToken(tokenID);
//		assertFalse(tokenManager.getTokenRegister().getStatusOfToken(tokenID));
//	}
//
//	@Then("^the token is rejected$")
//	public void theTokenIsRejected() throws Throwable {
//		assertFalse(response);
//	}
//
//	@Given("^a customer has a fake token$")
//	public void a_customer_has_a_fake_token() throws Throwable {
//		Customer customer = new Customer("Chris");
//		Token fakeToken = new Token();
//		tokenID=fakeToken.getTokenID();
//	}
//
//	@Given("^a name \"([^\"]*)\"$")
//	public void a_name(String customerName) throws Throwable {
//		name=customerName;
//	}
//
//	@When("^custumer is created$")
//	public void custumer_is_created() throws Throwable {
//		customer=new Customer(name);
//	}
//
//	@Then("^the name of the customer is \"([^\"]*)\"$")
//	public void the_name_of_the_customer_is(String customerName) throws Throwable {
//		assertEquals(customerName,customer.getName());
//	}
//	
//
//	@Then("^an id has been assigned$")
//	public void anIdHasBeenAssigned() throws Throwable {
//	    assertTrue(customer.getCustomerID()!="");
//	}
//
//
//	@Given("^a customer with less than (\\d+) unused token$")
//	public void a_customer_with_less_than_unused_token(int n) throws Throwable {
//		customer=new Customer("Dan");
//		tokenManager.getCustomerRegister().storeCustomer(customer);
//		numberOfTokens=customer.getTokens().size();
//		Iterator ti=(Iterator) customer.getTokens().iterator();
//		int numberOfUnusedTokens = 0;
//		while(ti.hasNext()) {
//			Token t= (Token) ti.next();
//			if(tokenManager.getTokenRegister().getStatusOfToken(t.getTokenID())) {
//				numberOfUnusedTokens+=1;
//			}
//		}
//		assertTrue(numberOfUnusedTokens<n);
//	}
//
//	@When("^custumer request (\\d+) tokens$")
//	public void custumer_request_tokens(int n) throws Throwable {
//		response=tokenManager.requestTokens(customer,n);
//	}
//
//	@Then("^the request is denied$")
//	public void the_request_is_denied() throws Throwable {
//		assertFalse(response);
//	}
//
//
//	@Given("^a customer with more than (\\d+) unused token$")
//	public void a_customer_with_more_than_unused_token(int n) throws Throwable {
//		customer=new Customer("Erik");
//		tokenManager.requestTokens(customer, 2);
//		Iterator ti=(Iterator) customer.getTokens().iterator();
//		int numberOfUnusedTokens = 0;
//		while(ti.hasNext()) {
//			Token t= (Token) ti.next();
//			if(tokenManager.getTokenRegister().getStatusOfToken(t.getTokenID())) {
//				numberOfUnusedTokens+=1;
//			}
//		}
//		assertTrue(numberOfUnusedTokens>n);
//	}
//
//	@Then("^the request is accepted$")
//	public void the_request_is_accepted() throws Throwable {
//		assertTrue(response);
//	}
//
//	@Then("^(\\d+) tokens are assigned to the customer$")
//	public void tokens_are_assigned_to_the_customer(int n) throws Throwable {
//		assertEquals(n+numberOfTokens,customer.getTokens().size());
//	}
//	
//	@Given("^a unregistered customer$")
//	public void aUnregisteredCustomer() throws Throwable {
//	   customer= new Customer("Finn");
//	   assertFalse(tokenManager.getCustomerRegister().containsCustomer(customer.getCustomerID()));
//
//	}
//
//	@Then("^the customer is registered$")
//	public void theCustomerIsRegistered() throws Throwable {
//	    assertTrue(tokenManager.getCustomerRegister().containsCustomer(customer.getCustomerID()));
//	}
//	
//
//
//
//}
//
