package  dtu.tokyo.task7.logic;

/**
 * @author Atli
 */

import javax.ws.rs.core.Response;

import dtu.tokyo.task7.adapter.db.token.TokenRegister;

public class TokenManager {
	private TokenRegister tokenRegister;

	public TokenManager(TokenRegister tr) {
		tokenRegister = tr;
	}

	public TokenRepresentation requestTokens(int n, String cprNumber) {
		TokenRepresentation tr = new TokenRepresentation("EMPTY", null);
		
		try {
			for(int i=0; i < n; i++) {
				Token t = new Token();		
				tokenRegister.storeToken(t.tokenID,t, cprNumber);
				tr = new TokenRepresentation(t.tokenID, new BarcodeGenerator().generateBarcode(t.tokenID));
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return tr;			
	}

	public Boolean validateToken(String tokenID) {
		return tokenRegister.getStatusOfToken(tokenID);
		
	}

	public String getCprNumber(String tokenID) {		
		return tokenRegister.getCprNumber(tokenID);
	}
}

