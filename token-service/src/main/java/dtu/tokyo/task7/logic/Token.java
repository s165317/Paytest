package  dtu.tokyo.task7.logic;

/**
 * @author Clara
 */

import java.security.MessageDigest;

public class Token {
	String tokenID;
	Boolean status;
	
	public Token() {
		String now = Long.toString(System.currentTimeMillis());
		
		this.tokenID = hash(now, "salty");
		this.status = true;
	}
	
    public static String hash(String password, String salt) {
    	try {
        MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
        StringBuilder sb = new StringBuilder();
        byte[] passBytes = (password + salt).getBytes();
        byte[] passHash = sha256.digest(passBytes);
        
        for(int i=0; i< passHash.length ;i++) {
            sb.append(Integer.toString((passHash[i] & 0xff) + 0x100, 16).substring(1));
        }
        String generatedPassword = sb.toString();
    	
        return generatedPassword;
    	}
    	catch(Exception e){
    		e.printStackTrace();
    	}
    	return "Hash_error";
    }
    
    public boolean isUsed() {
    	return this.status;
    }
    
    public String getTokenID() {
		return tokenID;
	}
	public void setTokenID(String id) {
		tokenID=id;
	}
	
	public Boolean getStatus() {
		return status;
	}
	
	public void setStatus(Boolean status) {
		this.status=status;
	}
	
}
