package  dtu.tokyo.task7.logic;

/**
 * @author Ioan
 */

import java.util.ArrayList;

public class Customer {
	private String firstname;
	private String lastname;
	private String cprNumber;
	private String account;
	private ArrayList<Token> tokens;
	
	public Customer(String firstname,String lastname) {
		tokens = new ArrayList<>();
		String now = Long.toString(System.currentTimeMillis());
		
		this.firstname = firstname;	
		this.lastname = lastname;
		this.cprNumber = (Token.hash(this.lastname + now, "salty"));
	}	

	public Customer() {
		
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getCprNumber() {
		return cprNumber;
	}

	public void setCprNumber(String cprNumber) {
		this.cprNumber = cprNumber;
	}

	public ArrayList<Token> getTokens() {
		return tokens;
	}

	public void setTokens(ArrayList<Token> tokens) {
		this.tokens = tokens;
	}
}
