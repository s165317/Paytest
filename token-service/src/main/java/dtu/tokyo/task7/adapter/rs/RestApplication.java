package dtu.tokyo.task7.adapter.rs;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author Atli
 */

@ApplicationPath("/")
public class RestApplication extends Application {

}
