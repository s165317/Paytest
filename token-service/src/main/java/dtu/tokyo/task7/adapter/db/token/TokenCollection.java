package dtu.tokyo.task7.adapter.db.token;

import java.util.HashMap;
import dtu.tokyo.task7.logic.Token;

/**
 * @author qinlong
 */

public class TokenCollection implements TokenRegister{
	// tokenID -> Token
	HashMap<String, Token> tokens;
	// tokenID -> cprNumber
	HashMap<String, String> tokensOwner;
	
	public TokenCollection() {
		tokens = new HashMap<>();
		tokensOwner = new HashMap<>();
	}
	
	@Override
	public void storeToken(String id, Token t, String cprNumber) {
		tokens.put(id,t);
		tokensOwner.put(id, cprNumber);
	}

	@Override
	public boolean getStatusOfToken(String tokenId) {
		return tokens.get(tokenId).isUsed();
	}

	@Override
	public boolean containsToken(String tokenID) {
		return tokens.containsKey(tokenID);
	}

	@Override
	public void setStatusOfToken(String tokenID, boolean b) {
		tokens.get(tokenID).setStatus(b);	
	}
	
	@Override
	public String getCprNumber(String tokenID) {
		return tokensOwner.get(tokenID);
	}
}
