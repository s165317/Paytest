package dtu.tokyo.task7.adapter.rs;

import javax.ws.rs.BadRequestException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.GET;

import dtu.tokyo.task7.adapter.db.token.TokenCollection;
import dtu.tokyo.task7.logic.*;

/**
 * @author Clara
 */

@Path("/tokens")
public class TokenServiceEndpoint {
	public static TokenManager tokenManager = new TokenManager(new TokenCollection());
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response requestTokens(@QueryParam("number") int number, @QueryParam("cprNumber") String cprNumber) {
		try {
			
			 TokenRepresentation response = tokenManager.requestTokens(number, cprNumber);
			
			return Response.status(201).entity(response).build();
		} catch (Exception e) {
			throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
		}
	}
	
	@Path("/test")
	@GET
	@Produces("text/plain")
	public Response doGet(){
		return  Response.ok("Hello there!").build();
	}
	
	
	@Path("/status")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response checkTokens(@QueryParam("tokenID") String tokenID) {
		
		try {
			String response=null;
			if(tokenManager.validateToken(tokenID)) {
				response=tokenManager.getCprNumber(tokenID);
				
			}
			return Response.status(201).entity(response).build();
			
		} catch (Exception e) {
			
		throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
		
		}
		
	}
	
	
//	@Path("/use/{tokenid}")
//	@POST
//	@Consumes(MediaType.TEXT_PLAIN)
//	@Produces(MediaType.TEXT_PLAIN)
//	public boolean useToken(@PathParam("tokenid") String tokenid) {
//		try {
//			return TokensResource.tokenManager.useToken(new String(tokenid));
//		} catch (Exception e) {
//			throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
//		}
//	}


}
