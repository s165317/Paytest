package dtu.tokyo.task7.adapter.db.token;

import dtu.tokyo.task7.logic.Token;

/**
 * @author Ioan
 */

public interface TokenRegister {
	public void storeToken(String id, Token t, String cprNumber);
	public boolean getStatusOfToken(String tokenId);
	public boolean containsToken(String tokenID);
	public void setStatusOfToken(String tokenID, boolean b);
	
	public String getCprNumber(String tokenID);
}
