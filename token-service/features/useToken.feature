Feature: Use of token
    
    Scenario: Use valid token
    Given a customer has a valid token
    When the customer use the token
    Then the token is accepted
    And the token is registered as used
    
    Scenario: Use used token 
    Given a customer has a used token
    When the customer use the token
    Then the token is rejected
 
   	Scenario: Use fake token
    Given a customer has a fake token
    When the customer use the token
    Then the token is rejected
    
 
