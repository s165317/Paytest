Feature: Requesting new tokens
 
  Scenario: Sucessful request for tokens
    Given a customer with less than 2 unused token
    When custumer request 3 tokens
    Then the request is accepted
    And 3 tokens are assigned to the customer 

  Scenario: Unsucessful request for tokens
    Given a customer with more than 1 unused token
    When custumer request 3 tokens
    Then the request is denied
    
  
	 Scenario: First request for tokens
    Given a unregistered customer
    When custumer request 3 tokens
    Then the request is accepted
    And 3 tokens are assigned to the customer  
    And the customer is registered  
  
  