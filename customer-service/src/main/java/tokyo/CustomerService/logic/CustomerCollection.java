package  tokyo.CustomerService.logic;

/**
 * @author Ioan
 */

import java.util.ArrayList;

import java.util.HashMap;

public class CustomerCollection implements CustomerRegister{
	HashMap<String, Customer> customerList;

	public CustomerCollection() {
		customerList = new HashMap<>();
	}

	@Override
	public void storeCustomer(Customer c) {
		customerList.put(c.getCprNumber(), c);
	}
	
	@Override
	public boolean updateCustomer(Customer c) {
		
		try {
			customerList.replace(c.getCprNumber(), c);
			return true;
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	public boolean containsCustomer(String customerID) {
		return customerList.containsKey(customerID);
	}
	
	@Override
	public Customer getCustomer(String customerID) {
		return customerList.get(customerID);
	}
	
	@Override
	public ArrayList<TokenRepresentation> retrieveTokens(String customerID){
		return customerList.get(customerID).getTokens();
	}
}
