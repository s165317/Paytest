package tokyo.CustomerService.logic;

/**
 * @author qinlong
 */

import java.util.ArrayList;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

public class CustomerManager {
	private CustomerRegister customers;

	public CustomerManager(CustomerRegister customerRegister) {
		this.customers  = new CustomerCollection();
	}

//	public CustomerRegister getCustomerList() {
//		return customerList;
//	}
//
//	public void setCustomerList(CustomerRegister customerRegister) {
//		this.customerList = customerRegister;
//	}

	public boolean registerCustomer(Customer customer) {
		try {
			customers.storeCustomer(customer);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public Customer updateCustomerTokens(String customerID, TokenRepresentation token) {
		String errorMsg = "NOT_INITALIZED";
		try {
			
			Customer tempCustomer = customers.getCustomer(customerID);
			//ArrayList<TokenRepresentation> tempList = tempCustomer.getTokens();
			ArrayList<TokenRepresentation> tempList = new ArrayList<>();
			
			errorMsg = tempCustomer.getCprNumber() + "  TempList size:"  + tempList.size() + "Update customer: ";  
			
			tempList.add(token);
			tempCustomer.setTokens(tempList);
			boolean temp = customers.updateCustomer(tempCustomer);
			
			
			//return "true";
			return tempCustomer;
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return new Customer();
	}
/////////////////// Not needed for current step (#3)
//    public boolean removeCustomer(Customer customer){
//        try{
//            customerRegister.r
//            return true;
//        }
//        catch (Exception e){
//            e.printStackTrace();
//        }
//        return false;
//    }

	public TokenRepresentation requestTokens(int number, String cprNumber) {
		Client c = ClientBuilder.newClient();
		WebTarget r = c.target("http://02267-tokyo.compute.dtu.dk:8585");
		
		Response request = r.path("tokens").queryParam("number",number).queryParam("cprNumber", cprNumber).request().post(Entity.json(null));
		TokenRepresentation requestResponse = request.readEntity(TokenRepresentation.class);
		request.close();
	    System.out.println("##############CustomerManagerTest#############:     " + requestResponse);
	    
	    return requestResponse;
	}

	public String requestAccount(String CPR) {
		String account=customers.getCustomer(CPR).getAccount();
		return account;
	}
}
