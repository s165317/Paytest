package tokyo.CustomerService.logic;

/**
 * @author Ioan
 */

import java.util.ArrayList;

public interface CustomerRegister {
	public void storeCustomer(Customer customer);
	public boolean updateCustomer(Customer customer);
	public boolean containsCustomer(String customerID);
	public Customer getCustomer(String customerID);
	public ArrayList<TokenRepresentation> retrieveTokens(String customerID); 
}
