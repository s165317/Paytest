package tokyo.CustomerService.rest;
/**
 * @author Ioan
 */

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import tokyo.CustomerService.logic.Customer;
import tokyo.CustomerService.logic.CustomerCollection;
import tokyo.CustomerService.logic.CustomerManager;
import tokyo.CustomerService.logic.TokenRepresentation;

@Path("/customer")
public class CustomerServiceEndpoint {
    private static CustomerManager customerManager = new CustomerManager(new CustomerCollection());

    @POST
    @Path("/registration")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response registerCustomer(Customer cr) {
        try {
            String response = Boolean.toString(customerManager.registerCustomer(cr));
            //System.out.println("############ PRINTING IN SERVER: " + response);
        	//boolean response = customerManager.registerCustomer(cr);
            //System.out.println("Server side: " + response);
            return Response.status(201).entity(response).build();

        } catch (Exception e) {
            throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
        }
    }
    
    @POST
    @Path("/tokens")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response requestTokens(Customer customer, @QueryParam("number") int number) {
        try {
            TokenRepresentation response = customerManager.requestTokens(number, customer.getCprNumber());
            
            /////////// UPDATE THE CUSTOMER IN THIS SERVER HERE/////////
            //String customerUpdate = customerManager.updateCustomerTokens(customer.getCprNumber(), response);
            Customer customerUpdate = customerManager.updateCustomerTokens(customer.getCprNumber(), response);
            ////////////////////////////////////////////////////////////
            
            return Response.status(201).entity(customerUpdate).build();

        } catch (Exception e) {
            throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
        }
    }
    
    @POST
    @Path("/account")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response requestAccount(String CPR) {
        try {
            String response = customerManager.requestAccount(CPR);
            return Response.status(201).entity(response).build();

        } catch (Exception e) {
            throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
        }
    }
    
    

//    @Path("/test")
//    @POST
//  //  @Produces("text/plain")
//    @Consumes(MediaType.APPLICATION_JSON)
//    public Response registerCustomer2(Customer cr){
//        try {
//            boolean response = customerManager.registerCustomer(cr);
//            //System.out.println("Server side: " + response);
//            return Response.status(201).entity(response).build();
//
//        } catch (Exception e) {
//            throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
//        }
//    }

    @Path("/test")
    @GET
    @Produces("text/plain")
    public Response doGet(){
        return  Response.ok("Hello there customer!").build();
    }
    
//    @Path("/test2")
//    @GET
//    @Produces("text/plain")
//    public Response doGet2(){
//    	Customer cr = new Customer("Hannah2","Christoffersen","180364-1791");
//    	String response = Boolean.toString(customerManager.registerCustomer(cr));
//        return  Response.ok(response).build();
//    }

//    @Path("/use/{tokenid}")
//    @POST
//    @Consumes(MediaType.APPLICATION_JSON)
//    @Produces(MediaType.APPLICATION_JSON)
//    public String useToken(@PathParam("tokenid") String tokenid) {
//        try {
//            return "The message is: " + tokenid;
//        } catch (Exception e) {
//            throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
//        }
//    }
}