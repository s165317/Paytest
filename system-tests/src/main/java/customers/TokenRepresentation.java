package customers;

/**
 * @author Atli
 */
import java.io.File;

public class TokenRepresentation {
	private String tokenID;
	private File barcode;	
	
	public TokenRepresentation(String tokenID, File barcode) {
	
		this.tokenID = tokenID;
		this.barcode = barcode;
	}    
	
	public TokenRepresentation() {
		
	}
    
    public String getTokenID() {
		return tokenID;
	}
	public void setTokenID(String id) {
		tokenID = id;
	}
	
	public File getBarcode() {
		return barcode;
	}
	
	public void setBarcode(File b) {
		barcode=b;
	}
}

