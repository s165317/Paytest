package customers;

/**
 * @author Ioan
 */
import java.util.ArrayList;

public class Customer {
    private String firstname;
    private String lastname;
    private String cprNumber;
    private String account;
    private ArrayList<TokenRepresentation> tokens;

    public Customer(String firstname, String lastname, String cprNumber){
        this.firstname = firstname;
        this.lastname = lastname;
        this.cprNumber = cprNumber;
    }

    public Customer(){

    }

    public void setCprNumber(String cprNumber) {
        this.cprNumber = cprNumber;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
    public void setAccount(String account) {
        this.account = account;
    }
    public void setTokens(ArrayList<TokenRepresentation> tokens){
    	this.tokens = tokens;
    }
    
    public String getFirstname() { return firstname; }

    public String getLastname() {
        return  lastname;
    }
    public String getCprNumber() {
        return cprNumber;
    }
    public String getAccount(){ return account;  }
    
    public ArrayList<TokenRepresentation> getTokens(){
    	return tokens;
    }

}

