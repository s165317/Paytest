package customers;
/**
 * @author qinlong
 */

import java.io.File;

import java.io.FileOutputStream;

import javax.imageio.ImageIO;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.Reader;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.oned.Code128Reader;
import com.google.zxing.oned.Code128Writer;

// References.
// https://www.youtube.com/watch?v=txmyS88iQHs
// https://downloads.aspose.com/barcode/java
// https://charli105patel.wordpress.com/2013/03/11/generate-barcode-using-zxing-library-in-java/
// https://mvnrepository.com/artifact/com.google.zxing/javase
//https://mvnrepository.com/artifact/com.google.zxing/core
// https://stackoverflow.com/questions/19208528/how-to-read-barcode-from-handheld-barcode-scanner-using-zxing-in-java

public class BarcodeGenerator {

//	private static String strBaseFolder = "./src/";

	
//		The following code can write QRcode
//		String text = "98376373783"; // this is the text that we want to encode  
//		int width = 400;  
//		int height = 300; // change the height and width as per your requirement  
		// (ImageIO.getWriterFormatNames() returns a list of supported formats)  
//		String imageFormat = "png"; // could be "gif", "tiff", "jpeg"   
//		BitMatrix bitMatrix = new QRCodeWriter().encode(text, BarcodeFormat.QR_CODE, width, height);  
//		MatrixToImageWriter.writeToStream(bitMatrix, imageFormat, new FileOutputStream(new File("qrcode_97802017507991.png")));

	// The Following code can write barcode
			//  Write Barcode
	public	File generateBarcode(String uniqueID) {
		try {
		//BitMatrix bitMatrix = new Code128Writer().encode(uniqueID, BarcodeFormat.CODE_128, 200, 100, null);
		BitMatrix bitMatrix = new Code128Writer().encode(uniqueID, BarcodeFormat.CODE_128, 400, 200, null);
		MatrixToImageWriter.writeToStream(bitMatrix, "png", new FileOutputStream(new File("./src/Barcode"+uniqueID+".png")));
		File barcode =  new File("./src/Barcode"+uniqueID+".png");
		return barcode;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return new File("./src/Barcode.png");
	}
	
	public	String decodeBarcode(File barcode) {
		String uniqueID = null;
		
		try {
		//BitMatrix bitMatrix = new Code128Writer().encode(uniqueID, BarcodeFormat.CODE_128, 200, 100, null);
		//BitMatrix bitMatrix = new Code128Writer().encode(uniqueID, BarcodeFormat.CODE_128, 400, 200, null);
		LuminanceSource source = new BufferedImageLuminanceSource(ImageIO.read(barcode));
		BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
		Reader reader = new MultiFormatReader();
		Result result = reader.decode(bitmap);
		uniqueID=result.toString();
		///////test print code ///////////
		System.out.println("Barcode String is " + uniqueID);
		return uniqueID;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return uniqueID;
		
	}
	
		

		
		
		
		
		
	
}
