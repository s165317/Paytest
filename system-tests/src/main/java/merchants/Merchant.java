package merchants;

/**
 * @author Clara
 */

public class Merchant {
	private String firstname;
    private String lastname;
    private String cprNumber;
    private String account;

    public Merchant(String firstname, String lastname, String cprNumber){
        this.firstname = firstname;
        this.lastname = lastname;
        this.cprNumber = cprNumber;
    }

    public Merchant(){

    }

    public void setCprNumber(String cprNumber) {
        this.cprNumber = cprNumber;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
    
    public void setAccount(String account) { 
    	this.account = account; 
	}

    public String getFirstname() { return firstname; }

    public String getLastname() {  return  lastname; }

    public String getCprNumber() { return cprNumber; }

    public String getAccount(){ return account;  }

}