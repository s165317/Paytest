package merchantSimulator;

/**
 * @author Clara
 */

import merchants.Merchant;

import static org.junit.Assert.assertEquals;

import java.io.File;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceService;

public class MerchantSimulatorTest {

    public MerchantSimulatorTest(){

    }

    public void registerMerchant(Merchant merchant)
    {
        Client c = ClientBuilder.newClient();
        WebTarget r = c.target("http://02267-tokyo.compute.dtu.dk:8383");

        Response request = r.path("merchant").path("registration").request().post(Entity.json(merchant));
        String requestResponse = request.readEntity(String.class);
        request.close();
	    System.out.println("##############MerchantSimulatorTest#############:     " + requestResponse);
        
        assertEquals(requestResponse,"true");
     }
    
    public String requestPayment(Merchant merchant,String tokenID, int amount) {
    	Client c = ClientBuilder.newClient();
        WebTarget r = c.target("http://02267-tokyo.compute.dtu.dk:8383");
        Response request = r.path("merchant").path("payment").queryParam("tokenID",tokenID).queryParam("amount", amount).request().post(Entity.json(merchant));
        String requestResponse = request.readEntity(String.class);	
        System.out.println("##############requestPaymentClientTest#############:     " + requestResponse);
        request.close();
        return requestResponse;

	}

//	public void checkPayment() {
//		BankService bank = new BankServiceService().getBankServicePort();
	
		
//	}
    
//    public void scanBarcode(File barcode) {
//    	Client c = ClientBuilder.newClient();
//        WebTarget r = c.target("http://02267-tokyo.compute.dtu.dk:8484");
//        Response request = r.path("merchant").request().post(Entity.json(barcode));
//        String requestResponse = request.readEntity(String.class);
//        request.close();
//	    System.out.println("##############MerchantSimulatorTest#############:     " + requestResponse);
//        
//        assertEquals(requestResponse,"true");
//    }

}



