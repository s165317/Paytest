package customerSimulator;

/**
 * @author Atli
 */

import customers.Customer;

import static org.junit.Assert.assertEquals;

import java.io.File;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class CustomerSimulatorTest {

	public CustomerSimulatorTest() {

	}

	public void registerCustomer(Customer customer) {
		Client c = ClientBuilder.newClient();
		WebTarget r = c.target("http://02267-tokyo.compute.dtu.dk:8484");

		Response request = r.path("customer").path("registration").request().post(Entity.entity(customer, MediaType.APPLICATION_JSON));
		String requestResponse = request.readEntity(String.class);
		request.close();
	    System.out.println("##############CustomerSimulatorTest#############:     " + requestResponse);

		assertEquals(requestResponse, "true");
	}
	
	public Customer requestTokens(Customer customer, int n) {
		Client c = ClientBuilder.newClient();
		WebTarget r = c.target("http://02267-tokyo.compute.dtu.dk:8484");

		Response request = r.path("customer").path("tokens").queryParam("number",n).request().post(Entity.json(customer));
		//String requestResponse = request.readEntity(String.class);
		Customer requestResponse = request.readEntity(Customer.class);
		request.close();
	    System.out.println("##############TokensRequest test#############:     " + requestResponse.getCprNumber());
	    
	    //assertEquals(requestResponse, "true");
	    
	    return requestResponse;
	}
	
	public void sendBarcode(File barcode) {
		Client c = ClientBuilder.newClient();
		WebTarget r = c.target("http://02267-tokyo.compute.dtu.dk:8383");

		Response request = r.path("merchant").path("barcode").request().header("Content-Disposition", "attachment; filename=\"" + barcode.getName() + "\"" ) .post(Entity.entity(barcode,MediaType.APPLICATION_OCTET_STREAM ));
		String requestResponse = request.readEntity(String.class);
		request.close();
		
	    System.out.println("##############SendBarcode test#############:     " + requestResponse);

	}
}
