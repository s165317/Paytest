import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import customerSimulator.CustomerSimulatorTest;
import customers.Customer;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceService;
import dtu.ws.fastmoney.User;
import merchantSimulator.MerchantSimulatorTest;
import merchants.Merchant;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;
/**
 * @author Ioan
 */

public class stepDefinitions {
    private BankService bank = new BankServiceService().getBankServicePort();
    private List<String> accounts = new ArrayList<>();
    private Customer customer;
    private CustomerSimulatorTest customerSimulator;
    private MerchantSimulatorTest merchantSimulator;
    private Merchant merchant;
    private String response;
    
    
    @After
    public void cleanup() throws Throwable{
        for (String a : accounts){
            bank.retireAccount(a);

        }
    }

    @Given("^a registered customer with a bank account$")
    public void a_registered_customer_with_a_bank_account() throws Throwable {
        customer = new Customer("Hannah","Christoffersen","180364-1796");
        User user = new User();
        user.setFirstName(customer.getFirstname());
        user.setLastName(customer.getLastname());
        user.setCprNumber(customer.getCprNumber());
        String account = bank.createAccountWithBalance(user,new BigDecimal(1000));
        System.out.println("customer account works!"+account);
        accounts.add(account);
        customer.setAccount(account);
        customerSimulator = new CustomerSimulatorTest();
        customerSimulator.registerCustomer(customer); 

    }

    @Given("^a registered merchant with a bank account$")
    public void a_registered_merchant_with_a_bank_account() throws Throwable {
        merchant = new Merchant("Søren","Lauridsen","021145-1351");
        User user =new User();
        user.setFirstName(merchant.getFirstname());
        user.setLastName(merchant.getLastname());
        user.setCprNumber(merchant.getCprNumber());
        String account = bank.createAccountWithBalance(user,new BigDecimal(1000));
        System.out.println("merchant account works!" + account);
        accounts.add(account);
        merchant.setAccount(account);
        merchantSimulator = new MerchantSimulatorTest();
        merchantSimulator.registerMerchant(merchant);
    }

    @Given("^the customer has one unused token$")
    public void the_customer_has_one_unused_token() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
    	
    	// is this enough? // weird path issue
    	customer = customerSimulator.requestTokens(customer, 1);
    }

    @When("^the merchant scans the token$")
    public void the_merchant_scans_the_token() throws Throwable {

    	//DONE
    }

    @When("^requests payment for (\\d+) kroner using the token$")
    public void requests_payment_for_kroner_using_the_token(int amount) throws Throwable {
        response=merchantSimulator.requestPayment(merchant,customer.getTokens().get(0).getTokenID(),amount);

    }

    @Then("^the payment succeeds$")
    public void the_payment_succeeds() throws Throwable {
    	assertEquals("true",response);
    	//Response.Status.NO_CONTENT,response.getStatusInfo().toEnum()
    }

    @Then("^the money is transferred from the customer bank account to the merchant bank account$")
    public void the_money_is_transferred_from_the_customer_bank_account_to_the_merchant_bank_account() throws Throwable {
    	assertEquals(new BigDecimal(900),bank.getAccount(customer.getAccount()).getBalance());
		assertEquals(new BigDecimal(1100),bank.getAccount(merchant.getAccount()).getBalance());
		assertEquals("100 kroner has been transfered",bank.getAccount(customer.getAccount()).getTransactions().get(0).getDescription());
		assertEquals("100 kroner has been transfered",bank.getAccount(merchant.getAccount()).getTransactions().get(0).getDescription());
    }

}
