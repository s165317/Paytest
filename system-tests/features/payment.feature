Feature: Payment
  Scenario: Simple payment
    Given a registered customer with a bank account
    And a registered merchant with a bank account
    And the customer has one unused token
    When the merchant scans the token
    And requests payment for 200 kroner using the token
    Then the payment succeeds
    And the money is transferred from the customer bank account to the merchant bank account